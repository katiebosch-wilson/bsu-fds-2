#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

git pull origin master
cd ../
. /usr/local/bin/start.sh jupyter notebook $*
